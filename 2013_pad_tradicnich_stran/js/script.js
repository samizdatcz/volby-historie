Highcharts.setOptions({
    lang: {
        decimalPoint: "," ,
        }
    });

Highcharts.chart('volby10', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Volby do Poslanecké sněmovny 2010'
    },
    subtitle: {
        text: 'Volební účast: 62,60 %'
    },
    credits: {
        enabled: false
    },    
    xAxis: {
        categories: [
                    'ČSSD',
                    'ODS',
                    'TOP 09',
                    'KSČM',     
                    'VV',                                   
                    'KDU-ČSL',
                    'SPOZ'
        ],
        crosshair: true,
        labels: {
            overflow: 'justify'
        }        
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Procent hlasů:'
        },
        labels: {
            overflow: 'justify',
            format: '{value} %'
        }
    },      
    plotOptions: {
        column: {
            pointPadding: 0.01,
            borderWidth: 0,
            colorByPoint: true,
            colors: ["orange", "blue", "purple", "red", "lightblue", "yellow", "pink"],            
            dataLabels: {
                enabled: true,
                formatter: function () {
                 return( this.y.toString().replace(".",",") + " %")
                }
            }
        },
    },
    tooltip: {
        formatter: function () {
          if        (this.y == "22.08") {this.mandatu = "56"; this.nazev = "Česká strana sociálně demokratická"}
          else if   (this.y == "20.22") {this.mandatu = "53"; this.nazev = "Občanská demokratická strana"}
          else if   (this.y == "16.70") {this.mandatu = "41"; this.nazev = "TOP 09"}            
          else if   (this.y == "11.27") {this.mandatu = "26"; this.nazev = "Komunistická strana Čech a Moravy"}
          else if   (this.y == "10.88") {this.mandatu = "24"; this.nazev = "Věci veřejné"}
          else if   (this.y == "4.39") {this.mandatu = "0"; this.nazev = "Křesťanská a demokratická unie – Československá strana lidová"}
          else if   (this.y == "4.33") {this.mandatu = "0"; this.nazev = "Strana práv občanů Zemanovci"}
          return('<b>' + this.nazev + '</b><br>' + this.y.toString().replace(".",",") + ' %<br>' + this.mandatu + " mandátů")  
        }
    },    
    legend: {
            enabled: false
    },  
    series: [{
        name: 'Odpovědí',
        data: [22.08, 20.22, 16.70, 11.27, 10.88, 4.39, 4.33]

    }]
});

Highcharts.chart('volby13', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Volby do Poslanecké sněmovny 2013'
    },
    subtitle: {
        text: 'Volební účast: 59,48 %'
    },
    credits: {
        enabled: false
    },    
    xAxis: {
        categories: [
                    'ČSSD',
                    'ANO 2011',
                    'KSČM',
                    'TOP 09',     
                    'ODS',                                   
                    'Úsvit',
                    'KDU-ČSL'
        ],
        crosshair: true,
        labels: {
            overflow: 'justify'
        }        
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Procent hlasů:'
        },
        labels: {
            overflow: 'justify',
            format: '{value} %'
        }
    },      
    plotOptions: {
        column: {
            pointPadding: 0.01,
            borderWidth: 0,
            colorByPoint: true,
            colors: ["orange", "lightblue", "red", "purple", "blue", "lightgreen", "yellow"],            
            dataLabels: {
                enabled: true,
                formatter: function () {
                 return( this.y.toString().replace(".",",") + " %")
                }
            }
        },
    },
    tooltip: {
        formatter: function () {
          if        (this.y == "20.45") {this.mandatu = "50"; this.nazev = "Česká strana sociálně demokratická"}
          else if   (this.y == "18.65") {this.mandatu = "47"; this.nazev = "ANO 2011"}
          else if   (this.y == "14.91") {this.mandatu = "33"; this.nazev = "Komunistická strana Čech a Moravy"}            
          else if   (this.y == "11.99") {this.mandatu = "26"; this.nazev = "TOP 09"}
          else if   (this.y == "7.72") {this.mandatu = "16"; this.nazev = "Občanská demokratická strana"}
          else if   (this.y == "6.88") {this.mandatu = "14"; this.nazev = "Úsvit přímé demokracie Tomia Okamury"}
          else if   (this.y == "6.78") {this.mandatu = "14"; this.nazev = "Křesťanská a demokratická unie – Československá strana lidová"}
          return('<b>' + this.nazev + '</b><br>' + this.y.toString().replace(".",",") + ' %<br>' + this.mandatu + " mandátů")  
        }
    },    
    legend: {
            enabled: false
    },  
    series: [{
        name: 'Odpovědí',
        data: [20.45, 18.65, 14.91, 11.99, 7.72, 6.88, 6.78]

    }]
});
title: "Jak si Zeman s Klausem rozdělili Česko<br><span class='podtitul'>Pět momentů, které otřásly českou politikou<br>1. díl: Opoziční smlouva (1998)</span>"
perex: "S blížícími se sněmovními volbami přichází čas ohlédnout se, co určovalo podobu těch minulých. Server iROZHLAS.cz proto přináší seriál Pět momentů, které otřásly českou politikou. Od pondělí do pátku se každý den dočtete o tom nejdůležitějším, co se na politické scéně v posledních dvaceti letech událo. Prvním z momentů je podpis takzvané opoziční smlouvy, která v roce 1998 zajistila díky podpoře ODS Václava Klause fungování menšinové vlády ČSSD Miloše Zemana."
authors: ["Michal Zlatkovský"]
published: "9. října 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/uploader/1000946-485742_171008-224314_zlo.jpg
coverimg_note: "Foto <a href='#'>ČTK</a>"
styles: []
libraries: ["https://code.highcharts.com/highcharts.src.js"]
options: "" #wide
---

_Přečtěte si také [druhý díl o zvolení Václava Klause prezidentem](https://www.irozhlas.cz/veda-technologie/historie/pithart-zeman-klaus-jak-se-hledal-prezident_1710101001_zlo)._

## Sarajevský atentát

Volební rok 1996 nebyl pro premiéra Václava Klause jednoduchý. Během čtyřleté vlády jím založené a vedené Občanské demokratické strany se mu podařilo mírově rozdělit Československo, provést Česko ekonomickou transformací a přihlásit zemi do Evropské unie a NATO. Stal se nejvýraznější figurou české politiky.

První Klausova vláda, která neměla v Poslanecké sněmovně vážnější opozici než zdiskreditovanou KSČM, se těšila mimořádné důvěře a Václav Klaus sám si uvnitř své strany držel silnou pozici. Ve volbách na jaře 1996 se proto očekávalo jasné vítězství ODS a pokračování její koalice s KDU-ČSL a Občanskou demokratickou aliancí.

O to překvapivější byly volební výsledky. České straně sociálně demokratické, která v předešlých volbách získala pouhých 16 mandátů, vlil krev do žil nový předseda, výrazný řečník Miloš Zeman. Pod jeho vedením se ČSSD, ve stále tvrdší opozici vůči ODS, stala druhou nejsilnější stranou a obsadila ve sněmovně 61 křesel – o sedm méně než vítězná ODS. 

<wide><div id="volby96"></div></wide>

Dosavadní koalice ODS, KDU-ČSL a ODA dosáhla jen na 99 mandátů z celkových dvou set a nebyla schopná složit většinu. Klaus svou druhou vládu sestavil jen díky dohodě se sociálními demokraty, za níž stál prezident Václav Havel. Sociální demokracie umožnila ODS získat důvěru výměnou za křeslo předsedy sněmovny pro Miloše Zemana. K tomu navíc oslabila pozice ODS ve vládní koalici - nová koaliční dohoda Klausovi zakazovala obcházet své partnery a schvalovat zákony za pomoci opozice, jak bylo občasným zvykem v jeho první vládě. Klaus se také často dostával do sporů s předsedou koaliční KDU-ČSL Josefem Luxem.

Nestabilní menšinová vláda se brzy dostala do problémů. Do té doby rychle rostoucí ekonomika vstoupila na přelomu let 1996 a 1997 do recese. Klaus, neústupný ve svém neoliberálním pohledu na ekonomiku, zareagoval „úspornými balíčky“ - škrty ve výdajích rozpočtu. To vyvolalo spory v koalici i mezi vládou a Českou národní bankou, která byla podle Klause hlavním viníkem nepříznivé ekonomické situace. Do té doby poměrně populární vláda začala ztrácet důvěru veřejnosti a Klausovi nezbylo než zareagovat výměnou některých ministrů.

<wide><div id="duveraKlaus"></div></wide>

Brzy však přichází další rána. Na podzim roku 1997 přerůstá podezření z nekalého financování ODS v jistotu. Podle výroční zprávy ODS měli mezi jejími největšími sponzory být dva neznámí cizinci: Radžív Sinha z Mauricia a Lájos Bács z Maďarska, kteří straně darovali celkem 7,5 miliónu. Podezření, že nikdo takový ve skutečnosti neexistuje, se potvrzuje v listopadu 1997, kdy se k darům přiznává Milan Šrejber, podnikatel, který se podílel na privatizaci Třineckých železáren.

O pouhé čtyři dny později přichází deník Mladá fronta Dnes s informací o tajném kontu ODS vedeném ve Švýcarsku, ze kterého měla strana nelegálně financovat své aktivity. Jen v roce 1996 musela ODS podle policie ze zahraničí [získat nejméně 90 miliónů korun](http://zpravy.idnes.cz/svycari-potvrdili-tajne-konto-ods-d5k-/domaci.aspx?c=A000713224501domaci_ond). Ještě ten den, zatímco je Václav Klaus na pracovní cestě v Sarajevu, vystupují před televizní kamery místopředsedové ODS Jan Ruml a Ivan Pilip a vyzývají Klause k odstoupení z čela strany. Média o události mluví jako o sarajevském atentátu.

<img src="https://interaktivni.rozhlas.cz/data/volby-historie/1998_opozicni_smlouva/media/ruml-pilip.jpg" width="100%">

Hned následující den podávají demisi ministři za KDU-ČSL i ODA a Klausův kabinet se rozpadá. Premiér následně podává do rukou Václava Havla demisi. Po několika týdnech jednání jmenuje prezident nového premiéra: dosavadního guvernéra ČNB Josefa Tošovského, který má spolu s poloúřednickou vládou dovést zemi k předčasným volbám. 

Klausova popularita se mezitím propadá na bod mrazu: podle průzkumu CVVM mu v prosinci nedůvěřuje 64,8 procent lidí. Tlaku na odstoupení ale odolá a na sjezdu ODS je opět zvolen předsedou strany. Klaus se s novými, věrnými místopředsedy distancuje od Tošovského vlády a svým vnitrostranickým odpůrcům dává ultimátum: buď opustí stranu, nebo vládu. Ruml, Pilip a další z ODS obratem odcházejí a zakládají novou stranu, Unii svobody.

## Úhlavní spojenci

Volbám v červnu 1998 předcházela nebývale tvrdá a agresivní kampaň mezi dvěma úhlavními nepřáteli, Zemanovou ČSSD a Klausovou ODS. ODS se ve snaze očistit jméno svého předsedy prohlásila za jedinou stálici české pravice a varovala před nástupem levice slogany jako <i>Mobilisace!</i> či <i>Doleva nebo s Klausem</i>. ČSSD na druhé straně barikády tvrdila, že Klausovy vlády po sobě zanechaly <i>spálenou zemi</i>, a slibovala „akci Čisté ruce“ - masivní protikorupční zátah proti privatizačním tunelářům.

<wide><div id="volby98"></div></wide>

ODS, které v prvních týdnech po sarajevském atentátu přisuzovaly průzkumy kolem 10 procent hlasů, slaví ve volbách překvapivý úspěch. Těsný poměr sil znamenal, že oba hlavní soupeři by s dvěma menšími stranami sestavili pohodlnou většinu. Do zdánlivě snadného vyjednávání se ovšem promítla osobní nevraživost. Spolupráci s ČSSD Miloše Zemana odmítla Unie svobody, případná menšinová vláda s komunisty byla pro vítěze voleb politicky neúnosná - jen o tři roky dříve přijala ČSSD takzvané Bohumínské usnesení, kterým si strana zakázala spolupráci s extremistickými stranami, mezi něž KSČM zařadila. Václav Klaus zase neodpustil lidovcům ani Unii svobody zradu z doby sarajevského atentátu a nabídl stranám tak nevýhodné podmínky, že spolupráci odmítly.

Miloš Zeman spolu s Václavem Klausem tak přistupují k nevídanému kroku a podepisují takzvanou opoziční smlouvu. Platnost dokumentu, celým názvem _Smlouvy o vytvoření stabilního politického prostředí v České republice uzavřené mezi Českou stranou sociálně demokratickou a Občanskou demokratickou stranou_, není omezená jedním volebním obdobím a oba předsedové potvrzují, že má platit „věčně“. Smlouva rozděluje důležité funkce mezi zástupce ČSSD a ODS a mimo jiné zavazuje ODS k nevyvolání a neúčasti na jakémkoliv hlasování o nedůvěře vládě. Miloš Zeman se stává premiérem, Václavu Klausovi připadá funkce předsedy Poslanecké sněmovny. Z úhlavních nepřátel jsou úhlavní spojenci.

## Koalice s opozicí

Opoziční smlouvu její kritici často označují za smlouvu koaliční. Jak ukázal mezi jinými [novinář Erik Tabery](http://www.ceskatelevize.cz/ivysilani/10123147075-vladneme-nerusit), tím, že ODS obsadila pozice v kontrolních výborech a dozorčích radách státních podniků v součinnosti s ČSSD, fakticky přestala plnit roli opozice. Zásadní rozhodnutí vlády Miloše Zemana padala za tichého souhlasu největší formálně opoziční strany - a přihlížení lidovců, Unie svobody i komunistů, kteří hráli roli převážně v méně závažných hlasováních. 

Václav Klaus i Miloš Zeman dodnes mluví o opoziční smlouvě jako o standardním politickém dokumentu, ve své době nutném pro stabilizaci země. Fakticky si obě strany za vzájemné podpory rozdělily funkce ve státních podnicích nebo obměňovaly vedení policie a tajných služeb.

Jedním z cílů opoziční smlouvy byla i změna volebního zákona tak, aby výrazně posílil strany, které ve volbách získají nejvíce hlasů. Podle úpravy, kterou poslanci v roce 2000 skutečně schválili, by vítězné straně stačilo kolem 30 procent hlasů k získání sněmovní většiny. ČSSD by tak podle tohoto zákona ve volbách v roce 1998 získala celkem 102 mandátů. Zákon však prezident Václav Havel napadl u Ústavního soudu, který všechny sporné body zrušil.

Opozičněsmluvní vládu Miloše Zemana provázela řada kauz. Patří mezi ně zahájení výstavby nejdražší české dálnice D47, kterou [vláda bez výběrového řízení svěřila](https://byznys.lidovky.cz/smutny-pribeh-d47-od-zemanova-izraelskeho-debaklu-po-rezivejici-mosty-129-/doprava.aspx?c=A131024_105557_ln-doprava_mev) izraelské firmě Housing and Construction za 125 miliard korun, nebo aféra Olovo - diskreditační materiál na tehdejší místopředsedkyni sněmovny za ČSSD Petru Buzkovou, který měl podle informací Mladé fronty Dnes kolovat mezi Zemanovými poradci. Dokument, pojmenovaný podle iniciálů Buzkové PB, obsahoval smyšlenky o její prostituci nebo spolupráci s StB. ČSSD později existenci spisu nepřímo přiznala a policie za jeho autora označila Zemanova poradce Vratislava Šímu. Případ následně uzavřela, protože čin neuznala jako společensky nebezpečný, a Šíma dál pracoval pro Ǔřad vlády.

Známá je i kauza plánované vraždy novinářky Sabiny Slonkové, kterou si objednal tehdejší sekretář ministerstva zahraničí Karel Srba. Slonková ve svých článcích pro Mladou frontu Dnes upozorňovala na Srbovo ovlivňování veřejných zakázek a na jeho majetkové poměry, které neodpovídaly příjmům státního úředníka. Srba byl za přípravu vraždy odsouzen na 12 let vězení.

Na přelomu let 2000 a 2001 pak došlo k takzvané televizní krizi, mocenskému boji ve veřejnoprávní České televizi, ze kterého vzešel širší politický spor. Rada ČT, zvolená ještě v předchozím sněmovním období, zvolila na začátku roku 2000 nového generálního ředitele Dušana Chmelíčka. Za rozhodnutí radu někteří poslanci kritizovali - tolik, že měsíc po volbě využili zákonnou možnost celý orgán odvolat a zvolit nový. Chmelíček se tak náhle zodpovídal zcela jiné radě, než která ho zvolila. 

<img src="https://interaktivni.rozhlas.cz/data/volby-historie/1998_opozicni_smlouva/media/tvkrize.jpg" width="100%">

Po měsících sporů Rada ČT Chmelíčka odvolala a na jeho místo dosadila bývalého ředitele zpravodajství ČT Jiřího Hodače. Toho řada redaktorů ČT považovala za blízkého ODS - zejména proto, že ve své předchozí funkci nechal po stížnosti Václava Klause odvolat moderátora diskusního pořadu V pravé poledne - a rozhodnutí vyvolalo vlnu odporu. Pracovníci zpravodajství ČT zakládají krizový výbor, vyzývají Hodače, aby funkci nepřevzal, a Radu ČT k rezignaci. 

Nový generální ředitel výzvu odmítá a na místo ředitelky zpravodajství jmenuje bývalou poradkyni Václava Klause Janu Bobošíkovou. Redaktoři zpravodajství její jmenování neuznávají, načež je jim Hodačem zabráněno vstoupit do velína zpravodajství. Po dobu dvou týdnů tak vznikají dvoje televizní zprávy: „oficiální“ Jany Bobošíkové připravované provizorním týmem, a „protestní“ od původních redaktorů, které je však dostupné pouze přes kabelové či satelitní vysílání. Bobošíková se protestujícím redaktorům pokouší rozdat výpovědi, ti je ovšem považují za neplatné.

Do krize se postupně vkládají i politici. Za Hodačem a Bobošíkovou stojí zejména ODS, za zbytkem pak zástupci lidovců a Unie svobody. Na podporu vzbouřených zaměstnanců se na Václavském náměstí koná demonstrace více než 100 tisíc lidí. Brzy poté Jiří Hodač na funkci ředitele rezignuje. Poslanecká sněmovna nakonec na výzvu Senátu odvolává i Radu ČT a mění zákon o České televizi tak, aby posílil její nezávislost. Situaci v ČT pak uklidňuje nový ředitel Jiří Balvín.

Opoziční smlouva se setkala také s občanským odporem. Jeho nejvýznamnějším projevem byla výzva Děkujeme, odejděte, která žádala demisi Miloše Zemana i Václava Klause. Výzvu podepsalo na 200 tisíc lidí, premiér i předseda sněmovny jí však odmítli věnovat výraznější pozornost.

<wide><div id="duveraZeman"></div></wide>

## Konec jedné éry

Přestože měla opoziční smlouva mít trvalou platnost, její období skončilo spolu s vládou Miloše Zemana. Ten ještě před koncem mandátu - v roce 2001, rok před volbami - oznámuje, že se nebude znovu ucházet o funkci předsedy ČSSD, a post přenechává svému ministru práce a odpůrci dalšího trvání opoziční smlouvy Vladimíru Špidlovi. Sám Zeman se na krátkou dobu stahuje do ústraní.

Špidlově ČSSD se daří s výrazným náskokem porazit Klausovu ODS a o pokračování opoziční smlouvy neuvažuje. Václav Klaus se stává řadovým poslancem a o funkci předsedy ODS se dál neuchází. Za jeho nejpravděpodobnějšího následníka straníci považují pozdějšího premiéra Petr Nečas - ale kongres ODS, který se koná několik měsíců po volbách, volí za předsedu rázného vsetínského senátora Mirka Topolánka. Klause zároveň prohlašuje za čestného předsedu a stranického kandidáta na prezidenta republiky.

<div id="spokojenost"></div>

Jak ukazují sociologické průzkumy, éra opoziční smlouvy měla zásadní vliv na občanské vnímání politiky. Deziluze z pragmatické spolupráce dříve znepřátelených stran zapříčinila nejen pokles důvěry v politiku jako takovou, ale také trvalé snížení volební účasti. Zatímco ještě v roce 1998 dorazilo k urnám přes 74 procent voličů, v roce 2002 to bylo jen 58 procent. Účast od té doby nepřesáhla dvoutřetinovou hranici. Smlouva samotná a její důsledky pro českou politiku zůstavají dodnes předmětem sporů politiků i odborné veřejnosti.
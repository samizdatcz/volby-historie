Highcharts.setOptions({
    lang: {
        decimalPoint: "," ,
        }
    });

Highcharts.chart('volby96', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Volby do Poslanecké sněmovny 1996'
    },
    subtitle: {
        text: 'Volební účast: 76,41 %'
    },
    credits: {
        enabled: false
    },    
    xAxis: {
        categories: [
                    'ODS',
                    'ČSSD',
                    'KSČM',
                    'KDU-ČSL',     
                    'SPR-RSČ',                                   
                    'ODA',
                    'DŽJ',
                    'DEU'
        ],
        crosshair: true,
        labels: {
            overflow: 'justify'
        }        
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Procent hlasů:'
        },
        labels: {
            overflow: 'justify',
            format: '{value} %'
        }
    },      
    plotOptions: {
        column: {
            pointPadding: 0.01,
            borderWidth: 0,
            colorByPoint: true,
            colors: ["blue", "orange", "red", "yellow", "#F0F0F0", "lightblue", "pink", "green"],            
            dataLabels: {
                enabled: true,
                formatter: function () {
                 return( this.y.toString().replace(".",",") + " %")
                }
            }
        },
    },
    tooltip: {
        formatter: function () {
          if        (this.y == "29.62") {this.mandatu = "68"; this.nazev = "Občanská demokratická strana"}
          else if   (this.y == "26.44") {this.mandatu = "61"; this.nazev = "Česká strana sociálně demokratická"}
          else if   (this.y == "10.33") {this.mandatu = "22"; this.nazev = "Komunistická strana Čech a Moravy"}            
          else if   (this.y == "8.08") {this.mandatu = "18"; this.nazev = "Křesťanská a demokratická unie – Československá strana lidová"}
          else if   (this.y == "8.01") {this.mandatu = "18"; this.nazev = "Sdružení pro republiku – Republikánská strana Československa"}
          else if   (this.y == "6.36") {this.mandatu = "13"; this.nazev = "Občanská demokratická aliance"}
          else if   (this.y == "3.09") {this.mandatu = "0"; this.nazev = "Důchodci za životní jistoty"}
          else if   (this.y == "2.8") {this.mandatu = "0"; this.nazev = "Demokratická unie"}
          return('<b>' + this.nazev + '</b><br>' + this.y.toString().replace(".",",") + ' %<br>' + this.mandatu + " mandátů")  
        }
    },    
    legend: {
            enabled: false
    },  
    series: [{
        name: 'Odpovědí',
        data: [29.62, 26.44, 10.33, 8.08, 8.01, 6.36, 3.09, 2.80]

    }]
});

Highcharts.chart('duveraKlaus', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Důvěra vládám Václava Klause'
    },
    subtitle: {
        text: 'Koalice ODS, KDU-ČSL a ODA (1993-96, 1996-98)'
    },
    credits: {
        text: 'zdroj: CVVM, Český sociálněvední datový archiv',
        href: 'http://archiv.soc.cas.cz/'
    },    
    xAxis: {
        categories: [
                    '1993',
                    '1994',
                    '1995',
                    '1996',     
                    '1997',                                   
                    '1997/12'
        ],
        crosshair: true,
        labels: {
            overflow: 'justify'
        }        
    },
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: 'Důvěřuje:'
        },
        labels: {
            overflow: 'justify',
            format: '{value} %'
        }
    },      
    plotOptions: {
        column: {
            pointPadding: 0.01,
            borderWidth: 0,          
            dataLabels: {
                enabled: true,
                formatter: function () {
                 return( this.y.toString().replace(".",",") + " %")
                }
            }
        },
    },
    tooltip: {
        enabled: false
    },
    legend: {
            enabled: false
    },  
    series: [{
        name: 'Důvěra:',
        data: [56.5, 54.1, 53.4, 48.4, 33.7, 13.3]
    }]
});

Highcharts.chart('duveraZeman', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Důvěra „opozičněsmluvní“ vládě Miloše Zemana'
    },
    subtitle: {
        text: 'Menšinová vláda ČSSD (1998-2002)'
    },
    credits: {
        text: 'zdroj: CVVM, Český sociálněvední datový archiv',
        href: 'http://archiv.soc.cas.cz/'
    },    
    xAxis: {
        categories: [
                    '1999',
                    '2000',
                    '2001',
                    '2002/5'
        ],
        crosshair: true,
        labels: {
            overflow: 'justify'
        }        
    },
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: 'Důvěřuje:'
        },
        labels: {
            overflow: 'justify',
            format: '{value} %'
        }
    },      
    plotOptions: {
        column: {
            pointPadding: 0.01,
            borderWidth: 0,          
            dataLabels: {
                enabled: true,
                formatter: function () {
                 return( this.y.toString().replace(".",",") + " %")
                }
            }
        },
    },   
    legend: {
            enabled: false
    },  
    tooltip: {
        enabled: false
    },    
    series: [{
        name: 'Důvěra:',
        data: [28.6, 30.6, 37.1, 42.3]
    }]
});

Highcharts.chart('volby98', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Volby do Poslanecké sněmovny 1998'
    },
    subtitle: {
        text: 'Volební účast: 74,03 %'
    },
    credits: {
        enabled: false
    },    
    xAxis: {
        categories: [
                    'ČSSD',
                    'ODS',
                    'KSČM',
                    'KDU-ČSL',     
                    'US',                                   
                    'SPR-RSČ',
                    'DŽJ'
        ],
        crosshair: true,
        labels: {
            overflow: 'justify'
        }        
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Procent hlasů:'
        },
        labels: {
            overflow: 'justify',
            format: '{value} %'
        }
    },      
    plotOptions: {
        column: {
            pointPadding: 0.01,
            borderWidth: 0,
            colorByPoint: true,
            colors: ["orange", "blue", "red", "yellow", "green", "#F0F0F0", "pink"],            
            dataLabels: {
                enabled: true,
                formatter: function () {
                 return( this.y.toString().replace(".",",") + " %")
                }
            }
        },
    },
    tooltip: {
        formatter: function () {
          if        (this.y == "32.31") {this.mandatu = "74"; this.nazev = "Česká strana sociálně demokratická"}
          else if   (this.y == "27.74") {this.mandatu = "63"; this.nazev = "Občanská demokratická strana"}
          else if   (this.y == "11.03") {this.mandatu = "24"; this.nazev = "Komunistická strana Čech a Moravy"}            
          else if   (this.y == "9.00") {this.mandatu = "20"; this.nazev = "Křesťanská a demokratická unie – Československá strana lidová"}
          else if   (this.y == "8.60") {this.mandatu = "19"; this.nazev = "Unie svobody"}
          else if   (this.y == "3.90") {this.mandatu = "0"; this.nazev = "Sdružení pro republiku – Republikánská strana Československa"}
          else if   (this.y == "3.06") {this.mandatu = "0"; this.nazev = "Důchodci za životní jistoty"}
          return('<b>' + this.nazev + '</b><br>' + this.y.toString().replace(".",",") + ' %<br>' + this.mandatu + " mandátů")  
        }
    },    
    legend: {
            enabled: false
    },  
    series: [{
        name: 'Odpovědí',
        data: [32.31, 27.74, 11.03, 9.00, 8.60, 3.90, 3.06]

    }]
});

Highcharts.chart('spokojenost', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Spokojenost s politickou situací'
    },
    credits: {
        text: 'zdroj: CVVM, Český sociálněvední datový archiv',
        href: 'http://archiv.soc.cas.cz/'
    },    
    xAxis: {
        categories: [
                    '1996',
                    '1999',
                    '2002',
        ],
        crosshair: true,
        labels: {
            overflow: 'justify'
        }        
    },
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: 'Spokojených:'
        },
        labels: {
            overflow: 'justify',
            format: '{value} %'
        }
    },      
    plotOptions: {
        column: {
            pointPadding: 0.01,
            borderWidth: 0,          
            dataLabels: {
                enabled: true,
                formatter: function () {
                 return( this.y.toString().replace(".",",") + " %")
                }
            }
        },
    },   
    legend: {
            enabled: false
    },  
    tooltip: {
        enabled: false
    },    
    series: [{
        name: 'Spokojených:',
        data: [43.2, 17.7, 33.4]
    }]
});
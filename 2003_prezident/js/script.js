Highcharts.setOptions({
    lang: {
        decimalPoint: "," ,
        }
    });

Highcharts.chart('volby02', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Volby do Poslanecké sněmovny 2002'
    },
    subtitle: {
        text: 'Volební účast: 58 %'
    },
    credits: {
        enabled: false
    },    
    xAxis: {
        categories: [
                    'ČSSD',
                    'ODS',
                    'KSČM',
                    'KDU-ČSL, US-DEU',     
                    'SN',                                   
                    'SZ'
        ],
        crosshair: true,
        labels: {
            overflow: 'justify'
        }        
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Procent hlasů:'
        },
        labels: {
            overflow: 'justify',
            format: '{value} %'
        }
    },      
    plotOptions: {
        column: {
            pointPadding: 0.01,
            borderWidth: 0,
            colorByPoint: true,
            colors: ["orange", "blue", "red", "yellow", "black", "green"],            
            dataLabels: {
                enabled: true,
                formatter: function () {
                 return( this.y.toString().replace(".",",") + " %")
                }
            }
        },
    },
    tooltip: {
        formatter: function () {
          if        (this.y == "30.2") {this.mandatu = "70"; this.nazev = "Česká strana sociálně demokratická"}
          else if   (this.y == "24.47") {this.mandatu = "58"; this.nazev = "Občanská demokratická strana"}
          else if   (this.y == "18.51") {this.mandatu = "41"; this.nazev = "Komunistická strana Čech a Moravy"}            
          else if   (this.y == "14.21") {this.mandatu = "31"; this.nazev = "KDU₋ČSL, Unie svobody - Demokratická unie"}
          else if   (this.y == "2.78") {this.mandatu = "0"; this.nazev = "Sdružení nezávislých"}
          else if   (this.y == "2.36") {this.mandatu = "0"; this.nazev = "Strana zelených"}
          return('<b>' + this.nazev + '</b><br>' + this.y.toString().replace(".",",") + ' %<br>' + this.mandatu + " mandátů")  
        }
    },    
    legend: {
            enabled: false
    },  
    series: [{
        name: 'Odpovědí',
        data: [30.20, 24.47, 18.51, 14.21, 2.78, 2.36]

    }]
});

Highcharts.chart('referendum', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Prezidentské referendum ČSSD'
    },
    subtitle: {
        text: '25 944 platných hlasů'
    },
    credits: {
        enabled: false
    },    
    xAxis: {
        categories: [
                    'Miloš Zeman',
                    'Jaroslav Bureš',
                    'Otakar Motejl',
        ],
        crosshair: true,
        labels: {
            overflow: 'justify'
        }        
    },
    yAxis: {
        min: 0,
        max: 100,
        title: {
            text: 'Hlasy'
        },
        labels: {
            overflow: 'justify',
            format: '{value} %'
        }
    },     
    colors: ["orange"], 
    plotOptions: {
        column: {
            pointPadding: 0.05,
            borderWidth: 0,       
            dataLabels: {
                enabled: true,
                formatter: function () {
                 return( this.y.toString().replace(".",",") + " %")
                }
            }
        },
    },
    tooltip: {
        enabled: false
    },
    legend: {
            enabled: false
    },  
    series: [{
        name: 'Hlasů',
        data: [49.5, 24.8, 20.5]
    }]
});

Highcharts.chart('kolo1', {
    chart: {
        type: 'column'
    },
    title: {
        text: '1. kolo první prezidentské volby'
    },
    subtitle: {
        text: 'K vítězství: 101 hlasů poslanců + 41 hlasů senátorů'
    },
    credits: {
        enabled: false
    },    
    colors: ["#5b55ff", "#ffd755"],
    xAxis: {
        categories: [
            'Václav Klaus',
            'Miroslav Kříženecký',
            'Jaroslav Bureš',
            'Petr Pithart',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 200,
        title: {
            text: 'Hlasů'
        }
    },
    tooltip: {
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'poslanců',
        data: [92, 44, 39, 20]

    }, {
        name: 'senátorů',
        data: [31, 2, 7, 35]

    }]
});

Highcharts.chart('kolo2', {
    chart: {
        type: 'column'
    },
    title: {
        text: '2. kolo první prezidentské volby'
    },
    subtitle: {
        text: 'K vítězství: 101 hlasů poslanců + 41 hlasů senátorů'
    },
    credits: {
        enabled: false
    },    
    colors: ["#5b55ff", "#ffd755"],
    xAxis: {
        categories: [
            'Václav Klaus',
            'Petr Pithart',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 200,
        title: {
            text: 'Hlasů'
        }
    },
    tooltip: {
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'poslanců',
        data: [77, 46]

    }, {
        name: 'senátorů',
        data: [32, 43]

    }]
});

Highcharts.chart('kolo3', {
    chart: {
        type: 'column'
    },
    title: {
        text: '3. kolo první prezidentské volby'
    },
    subtitle: {
        text: 'K vítězství: 141 hlasů poslanců a senátorů'
    },
    credits: {
        enabled: false
    },    
    colors: ["#5b55ff", "#ffd755"],
    xAxis: {
        categories: [
            'Václav Klaus',
            'Petr Pithart',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 200,
        title: {
            text: 'Hlasů'
        },
        plotLines: [{
            color: 'black',
            value: 141,
            width: 2
        }]
    },
    tooltip: {
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'poslanců',
        data: [80, 44]

    }, {
        name: 'senátorů',
        data: [33, 40]

    }]
});

Highcharts.chart('kolo4', {
    chart: {
        type: 'column'
    },
    title: {
        text: '1. kolo druhé prezidentské volby'
    },
    subtitle: {
        text: 'K vítězství: 101 hlasů poslanců + 41 hlasů senátorů'
    },
    credits: {
        enabled: false
    },    
    colors: ["#5b55ff", "#ffd755"],
    xAxis: {
        categories: [
            'Václav Klaus',
            'Miloš Zeman',
            'Jaroslava Moserová',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 200,
        title: {
            text: 'Hlasů'
        }
    },
    tooltip: {
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'poslanců',
        data: [89, 78, 25]

    }, {
        name: 'senátorů',
        data: [32, 5, 43]

    }]
});

Highcharts.chart('kolo5', {
    chart: {
        type: 'column'
    },
    title: {
        text: '2. kolo druhé prezidentské volby'
    },
    subtitle: {
        text: 'K vítězství: 101 hlasů poslanců + 41 hlasů senátorů'
    },
    credits: {
        enabled: false
    },    
    colors: ["#5b55ff", "#ffd755"],
    xAxis: {
        categories: [
            'Václav Klaus',
            'Jaroslava Moserová',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 200,
        title: {
            text: 'Hlasů'
        }
    },
    tooltip: {
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'poslanců',
        data: [85, 33]

    }, {
        name: 'senátorů',
        data: [33, 42]

    }]
});

Highcharts.chart('kolo6', {
    chart: {
        type: 'column'
    },
    title: {
        text: '3. kolo druhé prezidentské volby'
    },
    subtitle: {
        text: 'K vítězství: 141 hlasů poslanců a senátorů'
    },
    credits: {
        enabled: false
    },    
    colors: ["#5b55ff", "#ffd755"],
    xAxis: {
        categories: [
            'Václav Klaus',
            'Jaroslava Moserová',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 200,
        title: {
            text: 'Hlasů'
        },
        plotLines: [{
            color: 'black',
            value: 141,
            width: 2
        }]
    },
    tooltip: {
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'poslanců',
        data: [95, 26]

    }, {
        name: 'senátorů',
        data: [26, 39]

    }]
});

Highcharts.chart('kolo7', {
    chart: {
        type: 'column'
    },
    title: {
        text: '1. kolo třetí prezidentské volby'
    },
    subtitle: {
        text: 'K vítězství: 101 hlasů poslanců + 41 hlasů senátorů'
    },
    credits: {
        enabled: false
    },    
    colors: ["#5b55ff", "#ffd755"],
    xAxis: {
        categories: [
            'Václav Klaus',
            'Jan Sokol',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 200,
        title: {
            text: 'Hlasů'
        }
    },
    tooltip: {
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'poslanců',
        data: [115, 81]

    }, {
        name: 'senátorů',
        data: [32, 47]

    }]
});

Highcharts.chart('kolo8', {
    chart: {
        type: 'column'
    },
    title: {
        text: '2. kolo třetí prezidentské volby'
    },
    subtitle: {
        text: 'K vítězství: 101 hlasů poslanců + 41 hlasů senátorů'
    },
    credits: {
        enabled: false
    },    
    colors: ["#5b55ff", "#ffd755"],
    xAxis: {
        categories: [
            'Václav Klaus',
            'Jan Sokol',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 200,
        title: {
            text: 'Hlasů'
        }
    },
    tooltip: {
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'poslanců',
        data: [109, 83]

    }, {
        name: 'senátorů',
        data: [30, 46]

    }]
});

Highcharts.chart('kolo9', {
    chart: {
        type: 'column'
    },
    title: {
        text: '3. kolo třetí prezidentské volby'
    },
    subtitle: {
        text: 'K vítězství: 141 hlasů poslanců a senátorů'
    },
    credits: {
        enabled: false
    },    
    colors: ["#5b55ff", "#ffd755"],
    xAxis: {
        categories: [
            'Václav Klaus',
            'Jan Sokol',
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        max: 200,
        title: {
            text: 'Hlasů'
        },
        plotLines: [{
            color: 'black',
            value: 141,
            width: 2
        }]
    },
    tooltip: {
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            pointPadding: 0.1,
            borderWidth: 0
        }
    },
    series: [{
        name: 'poslanců',
        data: [109, 78]

    }, {
        name: 'senátorů',
        data: [33, 46]

    }]
});
title: "Pithart? Zeman? Klaus! Jak se hledal prezident<br><span class='podtitul'>Pět momentů, které otřásly českou politikou<br>2. díl: Volba prezidenta (2003)</span>"
perex: "S blížícími se sněmovními volbami přichází čas ohlédnout se, co určovalo podobu těch minulých. Server iROZHLAS.cz proto přináší seriál Pět momentů, které otřásly českou politikou. Od pondělí do pátku se každý den dočtete o tom nejdůležitějším, co se na politické scéně v posledních dvaceti letech událo. Druhým z momentů je první 'pohavlovská' devítikolová prezidentská volba, která poslancům a senátorům zabrala více než měsíc."
authors: ["Michal Zlatkovský"]
published: "10. října 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/uploader/01_15_volba_preziden_171010-084630_zlo.jpg
coverimg_note: "Foto <a href='#'>Senát</a>"
styles: []
libraries: ["https://code.highcharts.com/highcharts.src.js"]
options: "" #wide
---
_Přečtěte si také [první díl o éře opoziční smlouvy](https://www.irozhlas.cz/veda-technologie/historie/jak-si-zeman-s-klausem-rozdelili-cesko_1710090800_zlo)._

## Dozvuky opoziční smlouvy

 Desetiletá éra Václava Klause a Miloše Zemana skončila - tedy aspoň zdánlivě. [Konec opoziční smlouvy](https://interaktivni.rozhlas.cz/data/volby-historie/1998_opozicni_smlouva/) přinesl do politiky čerstvý vítr. Ve volbách v červnu 2002 vítězí s ČSSD Zemanův nástupce, bývalý ministr práce Vladimír Špidla, který uzavírá koalici s uskupením malých stran známých z předchozích období - KDU-ČSL a Unie svobody s Demokratickou unií. Disponuje však jen těsnou, nestabilní většinou 101 hlasů.

<wide><div id="volby02"></div></wide>

Klausova ODS prohrává a Václav Klaus sám se poprvé ocitá ve skutečné opozici - jako řadový poslanec bez funkce ve vládě nebo vedení sněmovny. Na stranickém sjezdu, který se koná několik měsíců po volbách, znovu nekandiduje. Favoritem na nového předsedu ODS je Klausův místopředseda Petr Nečas, nakonec však o pouhých 11 hlasů vítězí rázný senátor za Ostravu Mirek Topolánek, kterého Klaus ale nemá příliš v lásce. Do médií se dostává jeho SMS z kongresu, kde svého nástupce označuje za „absolutně prázdného a falešného Topola“. Klause kongres jmenuje za čestného předsedu - a svého kandidáta v nadcházející volbě prezidenta. Obě hlavní strany a KDU-ČSL s US-DEU doplňují ještě komunisté, tradičně izolovaní v opozici.

## První pokus
Křehkost vládní koalice se projevila záhy, v první „pohavlovské“ prezidentské volbě v lednu 2003. Nástupce Václava Havla, kterému končilo druhé ze dvou možných pětiletých období v prezidentském úřadu, měla i přes debaty o uzákonění přímé volby prezidenta společně vybrat Poslanecká sněmovna a Senát.

ČSSD, rozdělená po nástupu Vladimíra Špidly na několik mocenských bloků, se na svém prezidentském kandidátovi dlouho nemůže shodnout. Křídlo stoupenců Špidlova předchůdce Miloše Zemana v čele s místopředsedou ČSSD Zdeňkem Škromachem otevřeně podporuje jeho nominaci na prezidentský úřad. Zeman sám však prohlašuje, že je na odchodu z politiky a zúčastní se až případné druhé volby. Skupina kolem mladého ministra vnitra Stanislava Grosse vyzývá exministra spravedlnosti Jaroslava Bureše, zatímco premiér Špidla prosazuje ombudsmana Otakara Motejla.

Sociální demokraté tak na konci listopadu 2002 pořádají vnitrostranické referendum. Jasně v něm vítězí Miloš Zeman, který ale trvá na své neúčasti v první volbě. Protizemanovské vedení ČSSD proto nominuje druhého Jaroslava Bureše. Otakaru Motejlovi, kterého veřejnost podle průzkumů považuje za nejvhodnějšího Havlova nástupce, se cesta na Hrad uzavírá.

<div id="referendum"></div>

ČSSD v celém procesu obchází své koaliční partnery, kteří přitom původně stáli o společného vládního kandidáta. Předseda US-DEU Karel Kühnl označuje referendum sociálních demokratů za maškarádu a uskupení staví spolu s lidovci vlastního kandidáta - dlouholetého senátora Petra Pitharta. ODS podle očekávání vysílá do volby Václava Klause, KSČM právníka Miroslava Kříženeckého.

_Před zavedením přímé prezidentské volby volili společně prezidenta poslanci a senátoři. Volilo se ve Španělském sále Pražského hradu. Jedna volba mohla být maximálně tříkolová. Z prvního kola, kterého se účastnili kandidáti všech vládních stran, postoupili dva: ten, který dostal nejvíce hlasů od poslanců, a ten s nejvíce hlasy od senátorů. Ve druhém kole, tedy souboji nanejvýš dvou kandidátů, už kandidát potřeboval nadpoloviční většinu poslaneckých nebo senátorských hlasů. Pokud by kandidát v prvním nebo druhém kole získal obě většiny, zvítězil by rovnou. V opačném případě se pokračovalo třetím kolem, kde se hlasy poslanců a senátorů sčítaly. K vítězství tedy stačila nadpoloviční většina všech hlasů. Pokud nikdo dostatek hlasů nezískal, musela se uspořádat nová volba. Prezident se mohl volit veřejně i tajně, tedy „anonymně“. V roce 2003 se parlament shodl na tajném hlasování, což bylo pro podobu volby klíčové._

<div id="kolo1"></div>

První tříkolová prezidentská volba se koná 15. ledna 2003. Rozpolcenost ČSSD se projevuje drtivě: ačkoliv je přítomno 70 sociálnědemokratických poslanců, „jejich“ kandidát dostává v prvním kole pouze 39 poslaneckých hlasů. Do druhého kola postupuje Václav Klaus, kterého kromě zástupců ODS překvapivě volí i poslanci koaličních stran, zřejmě ČSSD a US-DEU, a Petr Pithart, který získává největší důvěru senátorů. Kandidát největší vládní strany tím zcela pohořel.

<div id="kolo2"></div>

V druhém kole vítězí mezi poslanci se 77 hlasy opět Klaus, stejný počet poslanců se však volby zdržel. Petr Pithart získává potřebnou většinu mezi senátory, chybí mu ale podpora koaličních stran. Pithart to označuje za chybu sociální demokracie. Třetí kolo dopadá téměř nachlup stejně a nezbývá tedy než celou volbu opakovat. ODS slaví debakl koalice: „Děkuji všem, předem i těm, kdo mě nevolili. Předem proto, neboť doufám, že se příště polepší,“ glosuje volbu Klaus. Předseda ODS Mirek Topolánek označuje neschopnost vládních stran dohodnout se na společném kandidátovi za světovou raritu. Zpravodajský web iDnes.cz celé sezení vystihuje lakonickým titulkem: Volba nemá vítěze, Zeman je ve hře.

<div id="kolo3"></div>

## Útěk Miloše Zemana
Čas na opakování volby prezidenta přichází 24. ledna 2003. Za ČSSD tentokrát i přes nelibost stranického vedení kandiduje Miloš Zeman, který tak plní svůj záměr účastnit se až druhé volby. ODS se drží Václava Klause, lidovci spolu s US-DEU nominují senátorku Jaroslavu Moserovou a komunisté vlastního kandidáta nestaví s tím, že budou hlasovat podle svého přesvědčení.

<div id="kolo4"></div>

Miloš Zeman dělá vše pro to, aby šel do volby jako favorit. Podle [informací Mladé fronty Dnes](http://zpravy.idnes.cz/zeman-sel-do-volby-jako-favorit-dmu-/domaci.aspx?c=A030123_222304_prezident_pol) přesvědčuje o své vhodnosti nejen koaliční kluby a ODS, ale také komunisty. V rámci vyjednávání prohlašuje i to, že považuje takzvané Bohumínské usnesení, které zakazuje ČSSD spolupráci s KSČM, za vyčpělé.

O to překvapivější je, když do dalšího kola nepostupuje. Podpora mu chybí zejména v rozdělené sociální demokracii. „Spor v ČSSD byl o tom, že Zeman by si na Hrad přivedl Šloufy a podobné lidi,“ komentuje situaci předseda sněmovny Lubomír Zaorálek. Zeman ze Španělského sálu kvapně odchází zadním vchodem a na další roky se stahuje do ústraní. Pro Špidlovo protizemanovské stranické křídlo jde naopak o částečné vítězství.

<div id="kolo5"></div>

V souboji s Jaroslavou Moserovou má výrazně navrch Václav Klaus, ve druhém ani třetím kole ale nedosahuje na dostatečný počet hlasů - Klaus vítězí u poslanců, Moserová u senátorů. Je jasné, že k volbě bude muset dojít znovu. V koalici se mluví o nutnosti postavit pro příští volbu vlastního společného kandidáta, Václav Klaus také zůstává neústupný: „Nenechám se odradit, jsem připraven nastoupit,“ ujišťuje zákonodárce. Na popud odcházejícího Václava Havla se diskutuje i o rychlém zavedení přímé volby, nakonec však dochází k v pořadí třetímu pokusu o zvolení prezidenta parlamentem.

<div id="kolo6"></div>

## Klausova chvíle

Třetí volba připadla až na 28. února - necelý měsíc po skončení mandátu Václava Havla a tedy téměř na konci zákonné třicetidenní lhůty pro konání prezidentské volby. Vládní koalice se konečně shoduje na společném kandidátovi, filosofovi Janu Sokolovi. Jeho jediným protikandidátem je stále odhodlaný Václav Klaus. „Připadám si jak v pohádce, kdy musím plnit tři úkoly, které jsou stále náročnější,“ komentuje své snažení v projevu před shromážděnými poslanci a senátory.

<div id="kolo7"></div>

Z prvního kola postupují oba kandidáti, Václav Klaus je přitom stejně jako při předchozích volbách úspěšnější než kandidát koalice. Od poslanců dostal 115 hlasů, a protože opozice disponovala 99 poslanci, muselo se na jeho stranu přiklonit alespoň 16 koaličních poslanců. Jan Sokol je úspěšnější u senátorů.

<div id="kolo8"></div>

Druhé kolo vypadá podobně - Klause podporuje i část koalice a Sokolovi hlasy poslanců nestačí. Spekuluje se o možné další volbě, ale parlament nakonec ve třetím kole za prezidenta volí Václava Klause. Získává 142 z 280 hlasů poslanců a senátorů - včetně hlasů z komunistické strany, která jednohlasně vystupuje proti Sokolovi. Vládní koalice tak definitivně prohrává. 

<div id="kolo9"></div>

## Definitivní (ne)vítězství
Dlouholeté „úhlavní přátelství“ Václava Klause a Miloše Zemana na pohled končí definitivním vítězstvím prvního z nich. Klaus obhajuje svůj mandát i v prezidentské volbě roku 2008 a podobu české politiky spoluvytváří dalších deset let. Úvahy o přímé volbě prezidenta však ke konci jeho funkčního období dojdou k naplnění - a dekádu po Klausovi se prezidentem díky hlasům voličů stává právě Miloš Zeman.

Předsednická kariéra Vladimíra Špidly je oproti tomu o poznání kratší. S roztříštěnou ČSSD dramaticky prohrává volby do Evropského parlamentu v roce 2004, když získává pouhých 8,78 procent hlasů. Záhy rezignuje na post předsedy strany i vlády. Nahrazuje ho nejmladší premiér v historii zemí EU, 34letý Stanislav Gross. Během jediného volebního období se nakonec u vlády vystřídají rekordní tři premiéři.
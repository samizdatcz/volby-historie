Highcharts.setOptions({
    lang: {
        decimalPoint: "," ,
        }
    });

Highcharts.chart('volby06', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Volby do Poslanecké sněmovny 2006'
    },
    subtitle: {
        text: 'Volební účast: 64,47 %'
    },
    credits: {
        enabled: false
    },    
    xAxis: {
        categories: [
                    'ODS',
                    'ČSSD',
                    'KSČM',
                    'KDU-ČSL',     
                    'SZ',                                   
                    'SNK-ED'
        ],
        crosshair: true,
        labels: {
            overflow: 'justify'
        }        
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Procent hlasů:'
        },
        labels: {
            overflow: 'justify',
            format: '{value} %'
        }
    },      
    plotOptions: {
        column: {
            pointPadding: 0.01,
            borderWidth: 0,
            colorByPoint: true,
            colors: ["blue", "orange", "red", "yellow", "green", "darkblue"],            
            dataLabels: {
                enabled: true,
                formatter: function () {
                 return( this.y.toString().replace(".",",") + " %")
                }
            }
        },
    },
    tooltip: {
        formatter: function () {
          if        (this.y == "35.38") {this.mandatu = "81"; this.nazev = "Občanská demokratická strana"}
          else if   (this.y == "32.32") {this.mandatu = "74"; this.nazev = "Česká strana sociálně demokratická"}
          else if   (this.y == "12.81") {this.mandatu = "26"; this.nazev = "Komunistická strana Čech a Moravy"}            
          else if   (this.y == "7.22") {this.mandatu = "13"; this.nazev = "Křesťanská a demokratická unie – Československá strana lidová"}
          else if   (this.y == "6.29") {this.mandatu = "6"; this.nazev = "Strana zelených"}
          else if   (this.y == "2.08") {this.mandatu = "0"; this.nazev = "SNK Evropští demokraté"}
          return('<b>' + this.nazev + '</b><br>' + this.y.toString().replace(".",",") + ' %<br>' + this.mandatu + " mandátů")  
        }
    },    
    legend: {
            enabled: false
    },  
    series: [{
        name: 'Odpovědí',
        data: [35.38, 32.32, 12.81, 7.22, 6.29, 2.08]

    }]
});